# Eyegog's Forge Minecraft Mod Collection
My collection of forge minecraft mods stored as `yaml`. And scripts/installers for my friends.

## Download these scripts
To get these scripts you need to download this repository:

1. If you are familiar with `git` then you already know what to do
2. If you aren't familiar with `git` or dont want to use git, you'll need to download this repository as an archive and extract it:
- On this page left to the 'Clone' button, click the download button and download it as a zip file (or your prefered archive choice)
- Once downloaded extract the archive contents using an archiver utility such as the [Windows 10 explorer](https://support.microsoft.com/en-us/windows/zip-and-unzip-files-8d28fa72-f2f9-712f-67df-f80cf89fd4e5), [7zip](https://www.7-zip.org/) or [winrar](https://www.win-rar.com/start.html?&L=0)

## Python
You will need to have python3 installed to run the scripts and a couple of packages.

#### Windows
##### Windows Store
Python 3.11 is available on the [Windows Store](https://apps.microsoft.com/detail/9nrwmjp3717k?hl=en-mt&gl=MT).

##### Python Installer
> **NOTE: Make sure you click 'Add python3 to PATH' during the installation**

Got to [the python downloads page](https://www.python.org/downloads/) and download the installer for the latest version of python3. Or [click here](https://www.python.org/ftp/python/3.10.6/python-3.10.6-amd64.exe) to download the installer for python 3.10.6

The `.bat` files will take care of the installation of python packages to run the scripts and will also run the scripts for you.

#### Linux
Install python3 as per your distribution.

Install `urllib3`

```bash
python3 -m pip install urllib3
```

## Minecraft client
### Forge
Mods are loaded via forge. [Click here](https://files.minecraftforge.net/net/minecraftforge/forge/) to download the forge installer of your desired version.

##### Windows
double-click the downloaded Java file to start the installer

1. Select 'Install client'
2. The installer show have the default path to the `.minecraft` folder. If you did not customise the minecraft installation then this is probably correct for you. If you installed minecraft to a different directory on your computer then you will need to point the installer to the appropriate place
3. Follow the installation process
4. When you next launch the minecraft client and go to the page for 'Minecraft: Java Edition', you should see 'forge <version>' to the left of the 'PLAY' button

##### Linux
Execute the forge java binary using `java -jar`:
```bash
java -jar <forge_binary.jar>
```

## Usage

> NOTE: The scripts will download the latest version of the mod list when they run

#### Windows
- Use `install_mods.bat` to install the mods listed in the mod files for your targeted version
- Use `remove_mods.bat` to remove the mods listed in the mod files for your targeted version
- Both scripts will try to find the default path (`C:\Users\[username]\AppData\Roaming\.minecraft\mods`) for windows, `$HOME/.minecraft/mods` for linux) and prompt you to use this folder
- If they don't find it, they will just prompt you for the path to the mods folder

#### Linux
Make sure you execute the scripts in the same directory as `forge_mods.yaml`
- `python3 download_forge_mods.py`: Download the mods listed in the mod files for your targeted version
- `python3 remove_forge_mods.py`: Remove the mods listed in the mod files for your targeted version
