#!/usr/bin/env python3
import os
import argparse
from utils import set_mod_path, get_mod_list, download_mods, download_shaders, remove_undefined_mods

def main():

    parser = argparse.ArgumentParser(
            prog = 'download_forge_mods.py',
            description = 'Downloads mods listed in forge_mods.yaml')

    parser.add_argument('-n', '--nofetch', action="store_true", default=False, help="Dont fetch modlist from gitlab")
    parser.add_argument('-r', '--noremove', action="store_true", default=False, help="Dont remove mods that are not defined in the modlist")

    args = parser.parse_args()

    version = input("Enter Minecraft version: ")

    mod_list = get_mod_list(version, not args.nofetch)
    mc_path, mods_path = set_mod_path()

    download_mods(mod_list, mods_path)
    download_shaders(version, mc_path)
    print()
    if not args.noremove:
        remove_undefined_mods(mod_list, mods_path)
    print("\nPraise be to eyegog!")
    input("Press any key to exit")

if __name__ == "__main__":
    main()
