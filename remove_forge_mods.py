import os
from utils import set_mod_path, get_mod_list
from urllib.parse import urlsplit

def main():

    version = input("Enter minecraft version: ")
    mod_list = get_mod_list(version, False)
    mods_path = set_mod_path()

    answer = input("\nAre you sure you want to proceed? [y/n]: ").lower()

    if answer != "y":
        print("Aborting...")
        exit(1)

    i = 0
    for mod in mod_list:
        split = urlsplit(mod.strip())
        filename = split.path.split("/")[-1]
        path = os.path.join(mods_path, filename)
        if os.path.exists(path):
            os.remove(path)
            print("Removed", filename)
            i += 1

    print("\n" + str(i) + " mods removed.")
    input("Press any key to exit")

if __name__ == "__main__":
    main()
