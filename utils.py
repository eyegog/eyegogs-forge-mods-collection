import os
import requests
import subprocess

def _get_url(url, path, verify=True):
    requests.packages.urllib3.disable_warnings()
    response = requests.get(url, verify=verify)
    if response.status_code == 200:
        with open(path, "wb") as file:
            file.write(response.content)
        return 0
    if response.status_code == 404:
        return -1
    else:
        print("Error retreiving url [" + str(response.status_code) + "]. Aborting...")
        exit(1)


def set_mod_path():
    mc_path = None
    if os.name == "nt":
        mc_path = "C:\\Users\\" + os.getlogin() + "\\AppData\\Roaming\\.minecraft"
        mods_path = mc_path + "\\mods"
    elif os.name == "posix":
        mc_path = "/home/" + os.getlogin() + "/.minecraft"
        mods_path = mc_path + "/mods"

    if not mc_path:
        print("Please provide path to minecraft mods folder")
        mc_path = input("Path: ")

    if os.path.exists(mc_path):
        if os.path.exists(mods_path):
            print("\nFound:", mods_path)
            answer = input("Is this the correct path to the minecraft mods folder? [y/n]: ").lower()
            if answer != "y":
                print("Aborting...")
                exit(1)
        else:
            answer = input("\nFound " + mc_path + " but mods folders does not exist. Create it? [y/n]: ").lower()
            if answer == "y":
                print("Creating directory:", mods_path + "...")
                os.mkdir(mods_path)
            else:
                print("Aborting...")
                exit(1)
    else:
        print(f"Path {str(mc_path)} is invalid or does not exist.")
        exit(1)

    return mc_path, mods_path

def download_mods(mod_list, mods_path):
    count = 1
    downloaded = 0
    mods_count = len(mod_list)

    print()

    for mod in mod_list:
        modstr = "[" + str(count) + "/" + str(mods_count) + "]"
        filename = mod.split("/")[-1]
        path = os.path.join(mods_path, filename)
        if os.path.exists(path):
            print(filename, "already exists. Skipping...", modstr)
        else:
            verify = True
            if "eyegog.co.uk" in mod:
                verify = False
            _get_url(mod, path, verify=verify)
            print("Downloaded " + filename + "...", modstr)
            downloaded += 1
        count += 1

    print("\n"+ str(downloaded) + " mods downloaded.")

def get_mod_list(version, fetch=True):
    server_mods_file = version + "_server_mods.txt"
    client_mods_file = version + "_client_mods.txt"
    shaders_file = version + "_shaders.txt"

    if fetch:
        print("Getting latest mod lists for version", version + "...")
        upstream_url = "https://gitlab.com/eyegog/eyegogs-forge-mods-collection/-/raw/master/"

        if _get_url(upstream_url + server_mods_file, server_mods_file) == 0:
            print("Got latest mod list for version", version)
        else:
            print("Error getting upstream mod list. Aborting...")
            exit(1)
        if _get_url(upstream_url + client_mods_file, client_mods_file) == 0:
            print("Got latest client mod list for version", version)
        else:
            print("Error getting upstream mod list. Aborting...")
            exit(1)


        if _get_url(upstream_url + shaders_file, shaders_file) == 0:
            print("Got latest shaders list for", version)
        else:
            print("No upstream shaders file found for version", version)

    mods = []
    with open(server_mods_file) as file:
        mods = file.readlines()
    with open(client_mods_file) as file:
        mods = mods + file.readlines()

    return [line.strip() for line in mods if line[0] != '#' ]

def remove_undefined_mods(mod_list, mods_path):
    mods = [item.split('/')[-1] for item in mod_list]
    for file in os.listdir(mods_path):
        if file[-4:] == ".jar" and file not in mods:
            os.remove(os.path.join(mods_path, file))
            print("Removed", file)

def download_shaders(version, mc_path):
    shaders_list = version + "_shaders.txt"
    shaders_path = os.path.join(mc_path, "shaderpacks")

    # Get if shaders file exists for this version
    if os.path.exists(shaders_list):
        print("\nFound shaders list for version: " + version + ". Downloading...")
        # Load in shaders file
        with open(shaders_list) as file:
            shaders = file.readlines()
            shaders = [line.strip() for line in shaders if line[0] != '#']
        # Create shaders folder if it doesnt exist
        if not os.path.exists(shaders_path):
            os.mkdir(shaders_path)
    
        shaders_count = len(shaders)
        count = 1
        downloaded = 0

        for shader in shaders:
            shaderstr = "[" + str(count) + "/" + str(shaders_count) + "]"
            filename = shader.split("/")[-1]
            this_path = os.path.join(shaders_path, filename)

            if os.path.exists(this_path):
                print("Shader", filename, "already exists. Skipping...", shaderstr)
            else:
                _get_url(shader, this_path)
                print("Downloaded shader " + filename + "...", shaderstr)
                downloaded += 1
            count += 1

        print("\n" + str(downloaded), "shaders downloaded.")
    else:
        print("\nNo shaders found list for version: " + version + ". Not installing.")
